## Studentenwerk Dresden canteen statistics
This repo is meant to be a starting point for deeper analysis of meals the Stuwe is selling.

The main focus lays on sustainability. 
It's initiated by the [AG Mensa (Tuuwi)](https://tuuwi.de/was-wir-machen/mensa/).

Two example questions are:
How many vegan meals does the Stuwe in Dresden offer compared to meals with meat? How is the distribution for the nice price? 

A detailed description of how to get the data for an analysis can be found in the _Docs_ folder:

[api_description.md](https://codeberg.org/tuuwi/mensa-analytics/src/branch/main/Docs/api_description.md)
